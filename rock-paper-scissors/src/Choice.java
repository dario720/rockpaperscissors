public enum Choice {
    ROCK,
    PAPER,
    SCISSORS;

    public static Choice pick(){
        int pickDouble = (int) Math.round(Math.random()*2);
        Choice result  = Choice.values()[pickDouble];
        return result;
    }
}



