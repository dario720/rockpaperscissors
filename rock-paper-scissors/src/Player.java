public class Player {
    private Choice playerchoice;
    private String name;
    public Player(String name){
        this.name = name;
    }
    public void generateChoice(){
        playerchoice = Choice.pick();
    }
   public Choice getChoice(){
        return playerchoice;
    }
    public String getName(){
        return name;
    }

}



