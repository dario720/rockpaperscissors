import sun.font.TrueTypeFont;

public class Game {
    private int rounds;
    private int player1Won;
    private int player2Won;
    private boolean hasWon;
    private Player player1;
    private Player player2;

    public Game(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
    }
    //
    public void start(){
        while(!hasWon) {
            generateHands();
            handComp();
            checkWinner();
            nextRound();
        }
    }
    private void handComp(){
        if(player1.getChoice().equals(player2.getChoice())){
            System.out.println("Its a draw");
        }else if(player1.getChoice() == Choice.ROCK && player2.getChoice() == Choice.PAPER) {
            System.out.println("Player 2 wins the round");
            player2Won++;
        }else if(player1.getChoice() == Choice.SCISSORS && player2.getChoice() == Choice.ROCK) {
            System.out.println("Player 2 wins the round");
            player2Won++;
        }else if(player1.getChoice() == Choice.PAPER && player2.getChoice() == Choice.SCISSORS) {
            System.out.println("Player 2 wins the round");
            player2Won++;
        }else {
            System.out.println("Player 1 wins the round");
            player1Won++;
        }
    }
    private void checkWinner() {
        if(player1Won == 2) {
            hasWon = true;
            System.out.println(player1.getName() + " wins the game!!! :D in " + rounds + " rounds");
        }else if(player2Won == 2) {
            hasWon = true;
            System.out.println(player2.getName() + " wins the game!!! :D in " + rounds + " rounds");
        }
    }


    private void nextRound(){
        rounds++;
    }
    private void generateHands(){
        player1.generateChoice();
        player2.generateChoice();
    }
}
