public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Mare");
        Player player2 = new Player("Dare");
        Game game = new Game(player1, player2);
        game.start();
    }
}